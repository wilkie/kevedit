# Clone emscripten
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk/
./emsdk install latest
./emsdk activate latest
source ./emsdk_env.sh
cd ..

# Configure
aclocal
autoconf
autoheader
automake --add-missing
emconfigure ./configure

# Build (needs no-implicit-* for strcmp in some files)
emmake make CFLAGS="-g -O2 -s USE_SDL=2 -Wno-implicit-function-declaration" OBJC=emcc OBJCFLAGS="-s USE_SDL=2 -Wno-implicit-function-declaration"

# Link
EMLDFLAGS="-O3 -s NO_EXIT_RUNTIME=1 -s EXPORTED_FUNCTIONS=[\"_main\"] -s EXTRA_EXPORTED_RUNTIME_METHODS=[\"FS\",\"ccall\",\"cwrap\",\"getValue\",\"setValue\"] -s ASSERTIONS=1 -s FORCE_FILESYSTEM=1"

# ASYNCIFY adds sleeps and delays to existing blocking functions
# For KevEdit, functions like SDL_WaitEvent block and would otherwise cause the
# browser to hang.
EMLDFLAGS="${EMLDFLAGS} -s ASYNCIFY=1"

# The different flags for different targets
EMLDFLAGS_ASMJS="${EMLDFLAGS} -s MODULARIZE=1 -s WASM=0 -s EXPORT_NAME=\"KevEdit\""
EMLDFLAGS_WASM="${EMLDFLAGS} -s MODULARIZE=1 -s WASM=1 -s ALLOW_MEMORY_GROWTH=1 -s EXPORT_NAME=\"KevEdit\""
EMLDFLAGS_HTML="${EMLDFLAGS} -s WASM=1 -s ALLOW_MEMORY_GROWTH=1"

# Linking steps
# Generates kevedit-template.html and kevedit-wasm{.js,.wasm}
ROOT=$PWD
cd src/kevedit
echo emcc -s USE_SDL=2 ${EMLDFLAGS_WASM} -o ../../kevedit-wasm.js kevedit.o main.o menu.o misc.o patbuffer.o screen.o  ../mkdtemp/mkdtemp.o  ../zlaunch/libzlaunch.a ../texteditor/libtexteditor.a ../dialogs/libdialogs.a ../help/libhelp.a ../themes/rusty/librusty.a ../synth/libsynth.a ../display/libdisplay.a ../structures/libstructures.a ../libzzt2/libzzt2.a ../../glob/libglob.a -lm
emcc -s USE_SDL=2 ${EMLDFLAGS_WASM} -o ../../kevedit-wasm.js kevedit.o main.o menu.o misc.o patbuffer.o screen.o  ../mkdtemp/mkdtemp.o  ../zlaunch/libzlaunch.a ../texteditor/libtexteditor.a ../dialogs/libdialogs.a ../help/libhelp.a ../themes/rusty/librusty.a ../synth/libsynth.a ../display/libdisplay.a ../structures/libstructures.a ../libzzt2/libzzt2.a ../../glob/libglob.a -lm
emcc -s USE_SDL=2 ${EMLDFLAGS_HTML} -o ../../kevedit-template.html kevedit.o main.o menu.o misc.o patbuffer.o screen.o  ../mkdtemp/mkdtemp.o  ../zlaunch/libzlaunch.a ../texteditor/libtexteditor.a ../dialogs/libdialogs.a ../help/libhelp.a ../themes/rusty/librusty.a ../synth/libsynth.a ../display/libdisplay.a ../structures/libstructures.a ../libzzt2/libzzt2.a ../../glob/libglob.a -lm
cd "${ROOT}"

# Make a copy of kevedit.js from the template to match our html
cp kevedit-template.js kevedit.js
